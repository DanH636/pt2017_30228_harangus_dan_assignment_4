package bank;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Bank implements BankProc,Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Nod hashtable [];
	
	static protected class Nod implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Account acc;
		Nod next;
		Nod prev;
		
		public Nod(Account acc)
		{
			this.acc=acc;
		}
	}
	
	public Bank()
	{
		hashtable = new Nod[30];
		this.loadSer();
		
	}
	
	public Nod[] getHashTabel()
	{
		return hashtable;
	}
	
	public int hashFunction(String id)
	{
		int suma=0;
		for (int i=0;i<id.length();i++)
		{
			suma=suma+id.charAt(i);
		}
		return suma%30;
	}
	
	
	private void loadSer()
	{
		try{
			FileInputStream fstream = new FileInputStream("textfile.ser");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strL;
			while ((strL=br.readLine())!=null)
			{
				String[] s;
				s=strL.split(" ");
				if (s[0].equals("bank.SavingAccount"))
					this.addAccount(new SavingAccount(s[4],new Person(s[1],s[2],s[3]),Integer.parseInt(s[5])));
				else
					this.addAccount(new SpendingAccount(s[4],new Person(s[1],s[2],s[3]),Integer.parseInt(s[5])));
			}
		}catch(Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	
	
	private boolean wellFormed()
	{
		for (int i=0;i<30;i++)
		{
			Nod end=hashtable[i];
			if (end!=null)
				while (end.next!=null)
					end=end.next;
			Nod start=hashtable[i];
			for (Nod p = start;p!=null;p=p.next)
			{
				if (p.prev!=null)
				{
					if (p.prev.next!=p)
						return false;
				}
				else
					if (start!=p)
							return false;
				if (p.next!=null)
				{
					if (p.next.prev!=p)
						return false;
				}
				else
					if (end!=p)
						return false;
			}
		}
		
		
		for (int i=0;i<30;i++){
			Nod end=hashtable[i];
			if (end!=null)
				while (end.next!=null){
					if (hashFunction(hashtable[i].acc.getID())==hashFunction(end.acc.getID()))
						end=end.next;
					else
						return false;
				}
		}
		
		
		for (int i=0;i<29;i++)
			for (int j=i+1;j<30;j++)
				if (hashtable[i]!=null && hashtable[j]!=null)
					if (hashFunction(hashtable[i].acc.getID())==hashFunction(hashtable[j].acc.getID()))
						return false;
		
		return true;
	}
	

	public void addAccount(Account acc) {
		
		assert (acc!=null) : "addAcount null";
		assert (this.findAccount(acc.getID())==null) : "account already exists";
	    assert  wellFormed();
		acc.addObserver(acc.getPers());
		String id = acc.getID();
		int n=this.hashFunction(id);
		Nod nod = new Nod(acc);
		if (hashtable[n]==null)
		{
			hashtable[n]=nod;
			nod.next=null;
			nod.prev=null;
		}
		else
		{
			nod.next=hashtable[n];
			nod.prev=null;
			hashtable[n].prev=nod;
			hashtable[n]=nod;
		}
		
		
		assert hashtable[n]==nod;
		assert wellFormed();
		acc.ownerNotify();
	
	}
	
	
	public void removeAccount(Account acc) {
		
		assert acc!=null;
		assert this.findAccount(acc.getID())!=null;
		assert wellFormed();
		
		Nod n1 = findAccount(acc.getID());
		Nod n2=hashtable[hashFunction(acc.getID())];
		if (n1.equals(n2))
		{
			hashtable[hashFunction(acc.getID())]=n1.next;
			if (n1.next!=null)
				hashtable[hashFunction(acc.getID())].prev=null;
		}
		else
		{
			n1.prev.next=n1.next;
			if (n1.next!=null)
				n1.next.prev=n1.prev;
			n1.prev=null;
			n1.next=null;
		}
		
		assert this.findAccount(acc.getID())==null;
		assert wellFormed();
		acc.ownerNotify();
	}
	
	public void updateAccount(Account acc,int sum) {
		
		assert acc!=null;
		assert this.findAccount(acc.getID())!=null : "contul nu exista";
		assert wellFormed();
		
		Nod n =findAccount(acc.getID());
		n.acc.makeDepozit(sum);
		
		assert this.findAccount(acc.getID())!=null;
		assert wellFormed();
		acc.ownerNotify();
	}
	
	public Nod findAccount(String accID)
	{
		Nod n=hashtable[hashFunction(accID)];
		while (n!=null)
		{
			if (accID.equals(n.acc.getID())==true)
				return n;
			n=n.next;
		}
		return null;
	}
	
	public String outPrint()
	{
		String s="";
		for (int i=0;i<30;i++)
			if (hashtable[i]!=null)
			{
				Nod n=hashtable[i];
				while(n!=null)
				{
					
					s=s.concat(n.acc.getClass().getName()+" "+n.acc.getPers().getId()+" "+n.acc.getPers().getLname()+" "+n.acc.getPers().getFname()+" "+n.acc.getID()+" "+Integer.toString(n.acc.getSum())+"\n");
					n=n.next;
				}
			}
		return s;
	}



	

	



	

	
}
