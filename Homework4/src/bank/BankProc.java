package bank;

public interface BankProc {
	
	/**
	 * 
	 * @param acount
	 * @pre acc!=null
	 * @pre findAccount(acc)==null
	 * @post hashtable[hashFunction(acc.getID())]=acc
	 */
	public void addAccount(Account acc);
	
	/**
	 * 
	 * @param acc
	 * @pre acc!=null
	 * @pre findAccount(acc)!=null
	 * @post findAccount(acc)==null
	 */
	public void removeAccount(Account acc);
	
	/**
	 * 
	 * @param acc
	 * @param sum
	 * @pre acc!=null
	 * @pre findAccount(acc)!=null
	 * @post @nochange
	 */
	public void updateAccount(Account acc, int sum);

}
