package bank;

public class SavingAccount extends Account{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SavingAccount(String ID, Person pers, int sum) {
		super(ID, pers, sum);
		// TODO Auto-generated constructor stub
	}
	
	public SavingAccount(String ID){
		super(ID);
	}
	
	public int getSum(){
		return sum;
	}

	@Override
	public boolean makeDepozit(int sum) {
		if (this.sum+sum<0)
			return false;
		this.sum=this.sum+sum;
		return true;
	}
	
	

}
