package bank;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class GUI extends JFrame implements ActionListener {


	private static final long serialVersionUID = 1L;
	private JButton create;
	private JButton delete;
	private JButton add;
	private JButton print;
	private JLabel labelLname;
	private JLabel labelFname;
	private JLabel labelIdNume;
	private JLabel labelIdAcc;
	private JLabel labelSum;
	private JTextArea textLname;
	private JTextArea textFname;
	private JTextArea textIdName;
	private JTextArea textIdAcc;
	private JTextArea textSum; JRadioButton savingAccount;
	private JRadioButton spendingAccount;
	private JPanel dataPanel;
	private JPanel rezPanel;

	private JScrollPane rezPane;
	private JTextArea rezultate;


	private Bank bank;

	public GUI() {
		this.setLayout(null);

		bank = new Bank();

		dataPanel = new JPanel();
		dataPanel.setLayout(null);
		dataPanel.setSize(350, 600);
		
		rezPanel = new JPanel();
		rezPanel.setLayout(new BorderLayout());
		rezPanel.setBounds(350, 50, 500, 450);
		rezPanel.setBackground(Color.RED);
		rezultate = new JTextArea();
		rezultate.setEditable(false);
		rezPane = new JScrollPane(rezultate);
		rezPanel.add(rezPane);
		

		labelIdNume = new JLabel("Id Name");
		labelIdNume.setBounds(80, 120, 70, 20);
		dataPanel.add(labelIdNume);

		labelLname = new JLabel("Last Name");
		labelLname.setBounds(80, 160, 70, 20);
		dataPanel.add(labelLname);

		labelFname = new JLabel("First Name");
		labelFname.setBounds(80, 200, 70, 20);
		dataPanel.add(labelFname);

		labelIdAcc = new JLabel("Id Account");
		labelIdAcc.setBounds(80, 240, 70, 20);
		dataPanel.add(labelIdAcc);

		labelSum = new JLabel("Sum");
		labelSum.setBounds(80, 280, 70, 20);
		dataPanel.add(labelSum);

		textIdName = new JTextArea();
		textIdName.setBounds(150, 120, 70, 20);
		dataPanel.add(textIdName);

		textLname = new JTextArea();
		textLname.setBounds(150, 160, 70, 20);
		dataPanel.add(textLname);

		textFname = new JTextArea();
		textFname.setBounds(150, 200, 70, 20);
		dataPanel.add(textFname);

		textIdAcc = new JTextArea();
		textIdAcc.setBounds(150, 240, 70, 20);
		dataPanel.add(textIdAcc);

		textSum = new JTextArea();
		textSum.setBounds(150, 280, 70, 20);
		dataPanel.add(textSum);

		savingAccount = new JRadioButton("SavingAccount");
		savingAccount.setBounds(50, 360, 150, 20);
		savingAccount.setSelected(true);
		dataPanel.add(savingAccount);

		spendingAccount = new JRadioButton("SpendingAccount");
		spendingAccount.setBounds(50, 420, 150, 20);
		dataPanel.add(spendingAccount);

		create = new JButton("Create");
		create.addActionListener(this);
		create.setBounds(200, 360, 90, 20);
		dataPanel.add(create);

		delete = new JButton("Delete");
		delete.addActionListener(this);
		delete.setBounds(200, 400, 90, 20);
		dataPanel.add(delete);
		
		add = new JButton("Add");
		add.addActionListener(this);
		add.setBounds(200,440,90,20);
		dataPanel.add(add);
		
		print = new JButton("Print");
		print.addActionListener(this);
		print.setBounds(200,480,90,20);
		dataPanel.add(print);

		this.add(dataPanel);
		this.add(rezPanel);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == create) {
			if (savingAccount.isSelected() == true) {
				bank.addAccount(new SavingAccount(textIdAcc.getText(), new Person(
						textIdName.getText(), textLname.getText(), textFname
												.getText()), Integer
						.parseInt(textSum.getText())));
			      try {
			          FileOutputStream fileOut = new FileOutputStream("textfile.ser");
			          ObjectOutputStream out = new ObjectOutputStream(fileOut);
			          out.writeObject(bank);
			          out.close();
			          fileOut.close();
			          System.out.printf("Serialized data is saved");
			       }catch(IOException i) {
			          i.printStackTrace();
			       }
			}
			else
				bank.addAccount(new SpendingAccount(textIdAcc.getText(), new Person(
						textIdName.getText(), textLname.getText(), textFname
												.getText()), Integer
						.parseInt(textSum.getText())));
		      try {
		          FileOutputStream fileOut = new FileOutputStream("textfile.ser");
		          ObjectOutputStream out = new ObjectOutputStream(fileOut);
		          out.writeObject(bank);
		          out.close();
		          fileOut.close();
		          System.out.printf("Serialized data is saved");
		       }catch(IOException i) {
		          i.printStackTrace();
		       }
			bank.outPrint();
			
			
		}
		if (e.getSource()==delete)
		{
			if (savingAccount.isSelected()==true){
				bank.removeAccount(new SavingAccount(textIdAcc.getText()));
			
		      try {
		          FileOutputStream fileOut = new FileOutputStream("textfile.ser");
		          ObjectOutputStream out = new ObjectOutputStream(fileOut);
		          out.writeObject(bank);
		          out.close();
		          fileOut.close();
		          System.out.printf("Serialized data is saved");
		       }catch(IOException i) {
		          i.printStackTrace();
		       }
			}
			else{
				bank.removeAccount(new SpendingAccount(textIdAcc.getText()));
			
		      try {
		          FileOutputStream fileOut = new FileOutputStream("textfile.ser");
		          ObjectOutputStream out = new ObjectOutputStream(fileOut);
		          out.writeObject(bank);
		          out.close();
		          fileOut.close();
		          System.out.printf("Serialized data is saved");
		       }catch(IOException i) {
		          i.printStackTrace();
		       }
		      
		      
			bank.outPrint();
			}
		}
		if (e.getSource()==add)
		{
			if (savingAccount.isSelected()==true){
				bank.updateAccount(new SavingAccount(textIdAcc.getText()), Integer.parseInt(textSum.getText()));
			
		      try {
		          FileOutputStream fileOut = new FileOutputStream("textfile.ser");
		          ObjectOutputStream out = new ObjectOutputStream(fileOut);
		          out.writeObject(bank);
		          out.close();
		          fileOut.close();
		          System.out.printf("Serialized data is saved");
		       }catch(IOException i) {
		          i.printStackTrace();
		       }
			}
			else{
				bank.updateAccount(new SpendingAccount(textIdAcc.getText()), Integer.parseInt(textSum.getText()));
			
		      try {
		          FileOutputStream fileOut = new FileOutputStream("textfile.ser");
		          ObjectOutputStream out = new ObjectOutputStream(fileOut);
		          out.writeObject(bank);
		          out.close();
		          fileOut.close();
		          System.out.printf("Serialized data is saved");
		       }catch(IOException i) {
		          i.printStackTrace();
		       }
		      
			bank.outPrint();
			}
			
		}
		if (e.getSource()==print)
		{
			rezultate.setText("");
			rezultate.append(bank.outPrint());
			
		}
		

	}

	public static void main(String[] args) {
		GUI g = new GUI();
		g.setSize(900, 600);
		g.setVisible(true);
		g.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
