package bank;

public class SpendingAccount extends Account{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpendingAccount(String ID, Person pers, int sum) {
		super(ID, pers, sum);
		// TODO Auto-generated constructor stub
	}
	public SpendingAccount(String ID){
		super(ID);
	}
	
	public int getSum(){
		return sum;
	}

	@Override
	public boolean makeDepozit(int sum) {
		if (this.sum+sum<-1000)
			return false;
		this.sum=this.sum+sum;
		return true;
	}
	
	

}
