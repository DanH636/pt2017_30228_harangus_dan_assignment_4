package bank;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id; 
	private String fname; 
	private String lname; 
	
	public Person (String id,String fname,String lname)
	{
		this.id=id;
		this.fname=fname;
		this.lname=lname;
	}
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id=id;
	}
	
	public String getFname()
	{
		return fname;
	}
	
	public void setFname(String fname)
	{
		this.fname=fname;
	}
	
	public String getLname()
	{
		return lname;
	}


	public void setLname(String lname)
	{
		this.lname=lname;
	}
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		System.out.println("Observed");
	}
	

}
