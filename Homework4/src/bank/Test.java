package bank;

import junit.framework.TestCase;

public class Test extends TestCase {

	//@Test
	public void testaddAcounts(){
		for (int i=0;i<50;i++){
			Bank bk = new Bank();
			Account ac = new SavingAccount("ABCD",new Person("ERT","Dan","Harangus"),1000);
			bk.addAccount(ac);
			assertTrue("Test",bk.findAccount(ac.getID())!=null);
		}
	}
	public void testremoveAcounts(){
		for (int i=0;i<50;i++){
			Bank bk = new Bank ();
			Account ac = new SavingAccount("DEFG",new Person("GHJ","Dan","Harangus"),300);
			bk.addAccount(ac);
			bk.removeAccount(ac);
			assertTrue("Test",bk.findAccount(ac.getID())==null);
		}
	}
}
