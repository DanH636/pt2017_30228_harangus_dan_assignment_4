package bank;

import java.util.Observable;
import java.io.Serializable;

abstract public class Account extends Observable implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ID;
	private Person pers;
	protected int sum;
	
	public Account(String ID, Person pers, int sum)
	{
		this.ID=ID;
		this.pers=pers;
		this.sum=sum;
	}
	public Account(String ID){
		this.ID=ID;
	}
	
	public String getID(){
		return ID;
	}
	
	public void setID(String ID){
		this.ID=ID;
	}
	
	public Person getPers(){
		return pers;
	}
	
	public void setPers(Person pers){
		this.pers=pers;
	}
	
	public int getSum(){
		return sum;
	}
	
	public void setSum(int sum){
		this.sum=sum;
	}
	
	abstract public boolean  makeDepozit(int sum);
	
	public void ownerNotify()
	{
		setChanged();
		notifyObservers();
	}

}
